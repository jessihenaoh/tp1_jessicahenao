import math
import itertools
from itertools import permutations

#exe.1
def isocele(a, b, c):
    if a==b or b==c or c==a:
        return True
    else: 
        return False
   
print (isocele(4, 2, 3))
print (isocele(4, 3, 3))
print (isocele(4, 4, 4))

#exe.2


def air_ordonne (a, b, c):
 vals = [a, b , c]
 u1 = min(vals)
 u3 = max(vals)
 vals.remove(u1)
 vals.remove(u3)
 u2 = vals[0]
 return 0.5 * math.sqrt(u1 * u1 * u3 * u3 - ((u1 * u1 - u2 * u2 + u3 * u3)/ 2)**2)

print (air_ordonne(4, 2, 3))
print (air_ordonne(4, 3, 3))
print (air_ordonne(4, 4, 4))
print (air_ordonne(3,4,5))
print (air_ordonne(13,14,15))
print (air_ordonne(1,1,1))


#exe.3

def definit_triangle (a, b, c):
  vals = 0
  if a>0 and b>0 and c>0:
    vals= (a + b + c)/2
    if a<vals and b<vals and c<vals: 
      return True
    else:
      return False
  else: 
    return False

print (definit_triangle(1, 1, 20))
print (definit_triangle(4, 2, 3))
print (definit_triangle(4, 4, 4))


#exe.4

def nb_triangles_speciaux (n, p):
    val = [*range(n, p)]
    opr = []
    i = 0
    x = 0
    print(val)
    vals=(list(itertools.combinations_with_replacement(val, 3)))
    print (vals)
    for i in vals:
      opr = i
      a = opr[0]
      b = opr[1]
      c = opr[2]
      per = sum(i)
      print (opr)
      verfication= (a + b + c)/2
      area = air_ordonne(a, b, c)
      if a>verfication and b>verfication and c>verfication and area == per:
        vals.remove(i)
        i = i+1

nb_triangles_speciaux(1, 20)

